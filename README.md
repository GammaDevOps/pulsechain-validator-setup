
Dependencies
OS requirements
Ubuntu Jammy 22.04 (LTS)

Install docker with the following instructions

https://docs.docker.com/engine/install/ubuntu/

Create Folders
mkdir -p blockchain/consensus
mkdir -p blockchain/execution
jwt.hex
cd blockchain
openssl rand -hex 32 | tr -d "\n" > "jwt.hex"
Validator_keys
Install dependencies
cd ~
git clone https://gitlab.com/pulsechaincom/staking-deposit-cli.git
cd staking-deposit-cli
sudo apt install python3-pip
sudo pip3 install -r requirements.txt
sudo python3 setup.py install
When generating validator keys, turn off the internet - Use an air-gapped computer!

Do not use sudo with the following command

Generate Validator Keys ( New-Mnemonic )
cd staking-deposit-cli
./deposit.sh new-mnemonic \
 --num_validators=1 \
 --chain=pulsechain \
 --folder=/home/<user>/blockchain \
 --eth1_withdrawal_address=<eth_address>
Generate Validator Keys ( Existing-Mnemonic )
cd staking-deposit-cli
./deposit.sh existing-mnemonic \
 --validator_start_index=10 \
 --num_validators=1 \
 --chain=pulsechain \
 --folder=/home/<user>/blockchain \
 --eth1_withdrawal_address=<eth_address>
—validator_start_index is the number of validators you ALREADY created.

—num_validators is the number of NEW validators you want to create.

If you try to deposit, and it shows 0 validators on the launchpad, then your permissions are wrong

Run the following, but update the path & file name.

sudo chmod user:user /path/to/deposit_*.json
Go-Pulse
Firewall rules
sudo ufw allow 30303
Go-pulse Run
See for the latest release - https://gitlab.com/pulsechaincom/go-pulse/-/releases

sudo docker run -d --restart unless-stopped \
 --net=host \
 --name=execution \
 -v /home/<changeusername>/blockchain:/blockchain \
 registry.gitlab.com/pulsechaincom/go-pulse:latest \
  --pulsechain \
  --authrpc.jwtsecret=/blockchain/jwt.hex \
  --datadir=/blockchain/execution \
  --http \
  --db.engine=leveldb
Notes: --db.engine=leveldb is required for NEW go-pulse installs, if you are currently running go-pulse you are already using leveldb and this flag isn’t required.

Prysm-Pulse
Firewall rules
sudo ufw allow 12000/udp
sudo ufw allow 13000/tcp
Prysm Wallet Password
cd blockchain
nano password.txt
Enter your Prysm wallet password, hit CRTL+X, hit y, hit enter

Prysm Beacon Run
See for the latest release - https://gitlab.com/pulsechaincom/prysm-pulse/-/releases

sudo docker run -d --restart unless-stopped --network=host --name=consensus -v /home/<changeusername>/blockchain:/blockchain registry.gitlab.com/pulsechaincom/prysm-pulse/beacon-chain:latest \
--pulsechain \
--jwt-secret=/blockchain/jwt.hex \
--datadir=/blockchain/consensus \
--execution-endpoint=http://127.0.0.1:8551 \
--checkpoint-sync-url=https://checkpoint.pulsechain.com \
--genesis-beacon-api-url=https://checkpoint.pulsechain.com \
--suggested-fee-recipient=<your_eth_address> \
--min-sync-peers=1
Prysm Validator keys import
See for the latest release - https://gitlab.com/pulsechaincom/prysm-pulse/-/releases

sudo docker run -it \
 --network=host \
 --name=validatorkeys \
 -v /home/<changeusername>/blockchain:/blockchain \
 registry.gitlab.com/pulsechaincom/prysm-pulse/validator:latest \
  accounts import \
  --pulsechain \
  --keys-dir=/blockchain/validator_keys \
  --wallet-dir=/blockchain/consensus/validator
Prysm Validator Run
See for the latest release - https://gitlab.com/pulsechaincom/prysm-pulse/-/releases

sudo docker run -d --restart unless-stopped \
 --network=host \
 --name=validator \
 -v /home/<changeusername>/blockchain:/blockchain \
 registry.gitlab.com/pulsechaincom/prysm-pulse/validator:latest \
  --pulsechain \
  --beacon-rpc-provider=127.0.0.1:4000 \
  --wallet-dir=/blockchain/consensus/validator \
  --wallet-password-file=/blockchain/password.txt \
  --datadir=/blockchain/consensus \
  --suggested-fee-recipient=<your_eth_address>
Lighthouse-pulse
Firewall rules
sudo ufw allow 9000
You may need to port forward 9000 on your local router

Lighthouse Validator Import
See for the latest release - https://gitlab.com/pulsechaincom/lighthouse-pulse/-/releases

sudo docker run -it \
 --net=host \
 --name=validatorkeys \
 -v /home/<changeusername>/blockchain/:/blockchain \
 registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest \
 lighthouse \
  account validator import \
  --network pulsechain \
  --directory /blockchain/validator_keys \
  --datadir=/blockchain/consensus
Lighthouse Beacon Run
See for the latest release - https://gitlab.com/pulsechaincom/lighthouse-pulse/-/releases

sudo docker run -d --restart unless-stopped \
 --net=host \
 --name=consensus \
 -v /home/<changeusername>/blockchain:/blockchain \
 registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest \
 lighthouse bn \
 --network=pulsechain \
 --execution-jwt=/blockchain/jwt.hex \
 --datadir=/blockchain/consensus \
 --execution-endpoint=http://127.0.0.1:8551 \
 --checkpoint-sync-url https://checkpoint.pulsechain.com \
 --suggested-fee-recipient=<your_eth_address> \
 --http
Lighthouse Validator Run
See for the latest release - https://gitlab.com/pulsechaincom/lighthouse-pulse/-/releases

sudo docker run -d --restart unless-stopped \
 --net=host \
 --name=validator \
 -v /home/<changeusername>/blockchain:/blockchain \
 registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest \
 lighthouse vc \
  --network=pulsechain \
  --datadir=/home/<changeusername>/blockchain/consensus \
  --suggested-fee-recipient=<your_eth_address>
Docker
Stop docker container
sudo docker stop -t 180 <containername>
Remove docker container
sudo docker rm <containername>
View Docker container logs
sudo docker logs --follow <containername>
List all images
sudo docker images ps -a
Remove images
sudo docker image rm <image_id>
ps to see processes (so works with containers)

ps -a to see stopped ones too

ls is to list so that it can be used with images (they don't have a process) or containers ... also add the -a flag for more detail.

Withdrawals
(below instructions are not fully tested, they probably don't work)

Lighthouse
https://lighthouse-book.sigmaprime.io/voluntary-exit.html

sudo docker run -it \
 --net=host \
 --name=validatorexit \
 -v /home/<changeusername>/blockchain:/blockchain \
 registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest \
 lighthouse \
  --network pulsechain \
  account validator exit \
  --keystore /blockchain/validator_keys/YOUR-keystore-m_XXXXXXXXXXXXXX.json \
  --beacon-node http://127.0.0.1:5052
Enter the validator key password

Enter "Exit my validator"

Prysm
https://docs.prylabs.network/docs/wallet/withdraw-validator

sudo docker run -it \
 --net=host \
 --name=validatorexit \
 -v /home/<changeusername>/blockchain/:/blockchain \
 registry.gitlab.com/pulsechaincom/prysm-pulse/prysmctl:latest \
  validator exit \
  --wallet-dir=/blockchain/consensus/validator \
  --beacon-rpc-provider=127.0.0.1:4000
You can also choose "All Accounts"

Enter "Exit my validator"

Set BLS withdrawal credentials
Not recommended to attempt until confirmation these steps work (work in progress).

( untested )

Create BLS withdrawal credentials
See links to official references:

https://launchpad.pulsechain.com/en/btec/

https://gitlab.com/pulsechaincom/staking-deposit-cli/#generate-bls-to-execution-change-arguments

https://launchpad.pulsechain.com/en/withdrawals

Example

./deposit.sh --language=english generate-bls-to-execution-change \
--chain=pulsechain \
--mnemonic="sister protect peanut hill ready work profit fit wish want small inflict flip member tail between sick setup bright duck morning sell paper worry" \
--bls_withdrawal_credentials_list="0x00bd0b5a34de5fb17df08410b5e615dda87caf4fb72d0aac91ce5e52fc6aa8de,0x00a75d83f169fa6923f3dd78386d9608fab710d8f7fcf71ba9985893675d5382" \
--validator_start_index=0 \
--validator_indices="50000, 50001" \
--execution_address="0x3434343434343434343434343434343434343434" \
--bls_to_execution_changes_folder=/home/<changeusername>/BLScreditentals
Broadcast withdrawal credentials to the beacon chain
You can broadcast your signed message from the command line using the curl command:

Prysm ( tested )
curl -X 'POST' \
  'localhost:3500/eth/v1/beacon/pool/bls_to_execution_changes' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d @filename.json
Check validator withdrawal creditientals

curl -X 'GET' \ 'http://127.0.0.1:3500/eth/v1/beacon/states/head/validators/<validator_index>' \
-H 'accept: application/json'
Lighthouse ( tested )
curl -X 'POST' \
  'localhost:5052/eth/v1/beacon/pool/bls_to_execution_changes' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d @filename.json
Check validator withdrawal creditientals

curl -X 'GET' \ 'http://127.0.0.1:5052/eth/v1/beacon/states/head/validators/<validator_index>' \
-H 'accept: application/json'